package org.ly817.endurance.config;

import org.ly817.endurance.entity.User;
import org.ly817.endurance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;

import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;

import java.util.Collection;

/**
 * webflux路由器函数（类似web中的controller） 配置
 * 实现响应式接口
 * 通过函数式编程的方式声明对外提供服务的接口
 * Spring 5之后，web MVC模块用于对外提供服务的接口进行封装
 * 由不同技术实现
 * Created by LuoYu on 2018/2/22.
 */
@Configuration
public class RouterFunctionConfiguration {
    /**
     * Servlet 原web实现
     * 请求接口：ServletRequest 或者 HttpServletRequest
     * 响应接口：ServletResponse 或者 HttpServletResponse
     *
     * Spring 5.0 重新定义了服务请求和响应接口，兼容servlet规范和reactive服务规范
     * 请求接口：ServerRequest
     * 响应接口：ServerResponse
     */
    @Bean
    @Autowired
    public RouterFunction<ServerResponse> personFindAll(UserRepository userRepository){
        Collection<User> users = userRepository.findAll();
        return RouterFunctions.route(RequestPredicates.GET("/person/find/all"),
                request -> {
                    /**
                     * webflux自定义集合类 （0表示兼容空指针）
                     * 与普通集合对象相比，自定义对象是异步处理（非阻塞），普通集合对象是同步处理（阻塞）
                     * Flux类：0~N个对象集合
                     * Mono类：0~1个对象集合
                     * Flux 和 Mono都是继承Publisher接口
                     * 这个接口reactive响应式编程
                     */
                    Flux<User> userFlux = Flux.fromIterable(users);
                    return ServerResponse.ok().body(userFlux,User.class);
                });


    }
}

package org.ly817.endurance.web;

import org.ly817.endurance.entity.User;
import org.ly817.endurance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RestController;

/**
 * Created by LuoYu on 2018/2/22.
 */
@RestController
public class UserController {

    private final UserRepository userRepository;

    /**
     * 使用构造器注入 autowired注解可不写
     * 好处：
     * 1.不能修改
     * 2.提前初始化
     * @param userRepository
     */
    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/person/save")
    public User save(@RequestParam String name){
        User user = new User();
        user.setName(name);
        if(userRepository.save(user)){
            System.out.printf("用户对象：%s 保存成功！",user.toString());
        }

        return user;
    }
}

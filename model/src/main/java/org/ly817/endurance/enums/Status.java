package org.ly817.endurance.enums;

/**
 * Created by LuoYu on 2018/3/3.
 */
public enum Status {




    //1×× Informational

    CONTINUE(100),

    SWITCHING_PROTOCOLS(101),

    PROCESSING(102),

    //2×× Success

    OK(200),

    CREATED(201),

    ACCEPTED(202),

    NON_AUTHORITATIVE_INFORMATION(203),

    NO_CONTENT(204),

    RESET_CONTENT(205),

    PARTIAL_CONTENT(206),

    MULTI_STATUS(207),

    ALREADY_REPORTED(208),

    IM_USED(226);

    private int code;

    Status(int code) {
        
        this.code = code;
    }



    public static void main(String[] args) {
        int i = 1;

    }
}

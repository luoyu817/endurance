package org.ly817.endurance.dao;

import org.ly817.endurance.entity.User;

/**
 * Created by LuoYu on 2018/3/4.
 */

public interface UserDao {
    int addUser(User user);
}
